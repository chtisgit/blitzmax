
#include "std.h"
#include "parser.h"
#include "output.h"
#include "config.h"

using namespace CG;

int main( int argc,char *argv[] ){

	stdutil_init( argc,argv );

	if( !opt_infile.size() ){
		cout<<"BlitzMax Release Version "<<BCC_VERSION<<endl;
		return 0;
	}

	if( !ftime(opt_infile) ) fail( "Input file not found" );

	bool t_debug=opt_debug;
	
	Parser parser;
	
	if( opt_verbose ) cout<<"Parsing..."<<endl;
	parser.parse();
	
	if( opt_verbose ) cout<<"Resolving types..."<<endl;
	Type::resolveTypes();
	
	if( opt_verbose ) cout<<"Resolving decls..."<<endl;
	Decl::resolveDecls();
	
	if( opt_verbose ) cout<<"Resolving blocks..."<<endl;
	Block::resolveBlocks();

	if( opt_verbose ) cout<<"Evaluating fun blocks..."<<endl;
	Block::evalFunBlocks();

	opt_debug=t_debug;
	opt_release=!opt_debug;

	if( opt_verbose ) cout<<"Generating assembly..."<<endl;
	FunBlock::genAssem();
	
	if( opt_verbose ) cout<<"Generating interface..."<<endl;
	FunBlock::genInterface();
	
	return 0;
}
