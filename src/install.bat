
cd ..
set BMXPATH=%CD%
cd src

rmdir /S /Q ..\bin
mkdir ..\bin
copy bin\fasm.exe ..\bin\


cd bmk2
mingw32-make clean install
cd ..

cd compiler
mingw32-make install
cd ..

..\bin\bmk makemods -a
..\bin\bmk makemods -a -h

..\bin\bmk makeapp -r -o ..\bin\makedocs.exe makedocs\makedocs.bmx
..\bin\bmk makeapp -r -o ..\MaxIDE.exe maxide\maxide.bmx

