#include "bmk.h"
#include "bmk_build.h"
#include "bmk_config.h"
#include "bmk_fs.h"
#include "bmk_util.h"
#include <cassert>
#include <stdexcept>
#include <array>
#include <algorithm>
#include <fstream>
#include <limits>
#include <unordered_set>

using namespace std;

vector<unique_ptr<SourceFile>> SourceFile::all;
vector<unique_ptr<Module>> Module::all;

Buildable::Buildable()
{
}
Buildable::~Buildable()
{
}
auto Buildable::ready(const OptionState& opt) const -> bool
{
	throw "Buildable::ready called";
}
auto Buildable::build(const OptionState& opt) -> void
{
	throw "Buildable::build called";
}
auto Buildable::sources() const -> std::vector<boostfs::path>
{
	throw "Buildable::sources called";
}
auto Buildable::outputs(const OptionState& opt) const -> std::vector<boostfs::path>
{
	throw "Buildable::outputs called";
}
auto Buildable::serialize(vector<Buildable*>&) -> bool
{
	throw "Buildable::serialize called";
}
auto Buildable::depends_on(const Module*) const -> bool
{
	throw "Buildable::depends_on called";
}

SourceFile::SourceFile(boostfs::path p) : path(p), mod(nullptr)
{
	if(!boostfs::is_regular_file(path)){
		throw invalid_argument("SourceFile::SourceFile is_regular_file");
	}
	
	ext = get_extensionl(path);
	ext.erase(0, 1);

	if(!valid_ext())
		throw invalid_argument("SourceFile::SourceFile valid_ext");
}
SourceFile::~SourceFile()
{
}

static const char ALNUM[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

auto SourceFile::valid_ext() -> bool
{
	static const unordered_set<string> valid {
		string("bmx"), string("i"), string("c"), string("m"), 
		string("h"), string("cpp"), string("cxx"), string("mm"),
		string("hpp"), string("hxx"), string("s"), string("asm")};

	return valid.count(ext) != 0;
}

auto SourceFile::parse() -> void
{
	if(parsed) return;
	parsed = true;
	ifstream src(path.string());

	//cdebug << "Parsing " << path.string() << " ..." << endl;

	if(!src){
		throw invalid_argument("SourceFile::parse could not open file");
	}
	
	string line;
	bool in_comment = false, cc = true;
	while(!src.eof()){
		getline(src, line);
		trim(line);
		
		if(ext == "bmx" || ext == "i"){
			auto lline = line;
			lower_string(lline);

			if(in_comment){
				if(lline.find("endrem") == 0 || lline.find("end rem") == 0){
					in_comment = false;
				}
				continue;
			}else if(lline.find("rem") == 0){
				in_comment = true;
				continue;
			}

			if(lline[0] == '?'){
				auto t = lline.substr(1);
				trim(t);

				bool cnot = false;
				if(t.find("not") == 0){
					cnot = true;
					t.erase(0, 3);
					trim(t);
				}

				if(t.empty()){
					cc = true;
				}else if(t == "debug"){
					cc = opt.debug;
				}else if(t == "threaded"){
					cc = opt.threaded;
				}else{
					cc = (t == opt.platform) || (t == opt.platform+opt.arch);
				}

				if(cnot) cc = !cc;
				continue;
			}

			if(cc == false) continue;

			if(!isalpha(lline[0])) continue;

			auto i = lline.find_first_not_of(ALNUM);
			if(i == string::npos) continue;

			auto key = lline.substr(0, i);
			auto val = line.substr(i);
			string qval, qext;
			trim(val);
			auto last_q = val.find('\"', 1);
			if (val.length() > 1 && val[0] == '\"' && last_q != string::npos){
				qval = val.substr(1, last_q - 1);
			}

			if(key == "module"){
				Module::find_one(val, &mod);
			}else if(key == "moduleinfo"){
				if(qval.find("CC_OPTS:") == 0 && mod != nullptr){
					mod->cc_opts += " "+qval.substr(8);
				}
			}else if(key == "framework"){
				if (!Module::find_one(val, &framework)){
					cerr << "source: " << path.string() << endl;
					cerr << "module : " << val << endl;
					// FIXME: correct error handling
					throw invalid_argument("SourceFile::parse no such module (framework)");
				}
			}else if(key == "import"){
				if(qval.length() > 0){
					
					if (qval[0] == '-'){
						if(mod != nullptr)
							mod->libs += " "+qval;
						else
							libs += " "+qval;
					}else{
						if(endswith(qval, ".h") || endswith(qval, ".hpp")){
							auto p = path.parent_path() / qval;
							if(boostfs::exists(p.parent_path()))
								mod->cc_opts += " -I"+p.parent_path().string();
						}else if(endswith(qval, ".o")){
							if(mod != nullptr)
								mod->blobs += " "+(path.parent_path() / qval).string();
						}else{
							auto *sf = SourceFile::add(path.parent_path() / qval);
							sf->mod = mod;
							imports.push_back(sf);
						}
					}
				}else{
					Module *m;
					lower_string(val);
					if(!Module::find_one(val, &m)){
						cerr << "source: " << path.string() << endl;
						cerr << "module : " << val << endl;
						// FIXME: correct error handling
						throw invalid_argument("SourceFile::parse no such module");
					}
					modimports.push_back(m);
				}
			}else if(key == "incbin"){
				if(qval.length() > 0){
					incbins.emplace_back(qval);
				}
			}else if(key == "include"){
				if (qval.length() > 0){
					includes.push_back(SourceFile::add(path.parent_path() / qval));
				}
			}else if (key == "moduleinfo"){
				if (qval.length() > 0){
					info.push_back(qval);
//					If mod_opts mod_opts.addOption(qval) ' BaH
				}
			}
		}else{
			if(line.find("#include") == 0){
				auto val = line.substr(8);
				
				if(val.length() > 1 && val[0] == '\"' && val[val.length() - 1] == '\"'){
					auto qval = val.substr(1, val.length() - 2);
					if (qval.length() > 0){
						includes.push_back(SourceFile::add(qval));
					}
				}
			}
		}
	}
}

auto SourceFile::ready(const OptionState& opt) const -> bool
{
	if(!boostfs::exists(output_path(opt)))
		return false;

	if(!boostfs::exists(path))
		return true;

	return boostfs::last_write_time(output_path(opt)) > boostfs::last_write_time(path);
}
auto SourceFile::makeC(const OptionState& opt) -> void
{
	auto dst = output_path(opt);
	string cflags = "-I"+modulepath().string();
	if(mod != nullptr)
		cflags += mod->cc_opts;
	if(opt.release)
		cflags += " -O3 -fno-tree-vrp -DNDEBUG";
	if(opt.threaded)
		cflags += " -DTHREADED";
	
	if(mod != nullptr){
		cflags += " -I"+mod->dir().string();
		for(auto *m : mod->mainsource()->modimports){
			cflags += " -I"+m->dir().string();
		}
	}
	cflags += " "+opt.cflags;

	exec(opt.cc+" "+cflags+" -c -o "+cquote(dst)+" "+cquote(path));
}
auto SourceFile::makeCpp(const OptionState& opt) -> void
{
	auto dst = output_path(opt);
	string cflags = "-I"+modulepath().string();
	if(mod != nullptr)
		cflags += mod->cc_opts;
	if(opt.release)
		cflags += " -O3 -fno-tree-vrp -DNDEBUG";
	if(opt.threaded)
		cflags += " -DTHREADED";
	
	if(mod != nullptr){
		cflags += " -I"+mod->dir().string();
		for(auto *m : mod->mainsource()->modimports){
			cflags += " -I"+m->dir().string();
		}
	}
	cflags += " "+opt.cflags;

	exec(opt.cxx+" "+cflags+" -c -o "+cquote(dst)+" "+cquote(path));
}
auto SourceFile::makebmx(const OptionState& opt) -> void
{
	auto dst = output_path(opt);
	string bcc_opts = " -g x86";
	if(opt.framework.size() > 2){
		bcc_opts += " -f "+opt.framework;
	}
	if(mod != nullptr)
		bcc_opts += " -m " + mod->modid();
	if(ismain){
		bcc_opts += " -t " + opt.apptype;
	}
	if(opt.release){
		bcc_opts += " -r";
	}
	if(opt.threaded){
		bcc_opts += " -h";
	}
	auto azm = dst;
	azm.replace_extension(".s");
	auto cmd = (get_bmxpath() / "bin" / "bcc").string();
	cmd += bcc_opts+" -o "+cquote(azm)+" "+cquote(path.string());
	boostfs::remove(azm);

	exec(cmd);
	if(!boostfs::exists(azm))
		throw std::runtime_error("compilation failed");
	
	assemble(azm, dst, opt);
}
auto SourceFile::assemble(const boostfs::path& src, const boostfs::path& dst, const OptionState& opt) -> void
{
	boostfs::remove(dst);
	exec(opt.as + " " + opt.asflags + " " +cquote(src) + " " + cquote(dst));
	if(!boostfs::exists(dst))
		throw runtime_error("compilation failed");
}
auto SourceFile::build(const OptionState& opt) -> void
{
	boostfs::create_directory(path.parent_path() / ".bmx");
	cout << "Compiling:" << path.filename().string() << endl;
	if(ext == "bmx")
		makebmx(opt);
	else if(ext == "c")
		makeC(opt);
	else if(ext == "cpp" || ext == "cxx" || ext == "cc" || ext == "C")
		makeCpp(opt);
	else if(ext == "asm" || ext == "s")
		assemble(path, output_path(opt), opt);
	else
		throw runtime_error("unknown source file type!");

}
auto SourceFile::sources() const -> vector<boostfs::path>
{
	vector<boostfs::path> v;
	v.push_back(path);
	return v;
}
auto SourceFile::outputs(const OptionState& opt) const -> vector<boostfs::path>
{
	vector<boostfs::path> v;
	v.push_back(output_path(opt));
	return v;
}
auto SourceFile::depends_on(const Module *m) const -> bool
{
	for(const auto *i : modimports){
		if(i == m) return true;
	}
	for(const auto *src : imports){
		if(src->depends_on(m)) return true;
	}
	return false;
}

auto SourceFile::output_path(const OptionState& opt) const -> boostfs::path
{
	auto name = path.filename().string();
	if(ismain) name += "."+opt.apptype;
	return path.parent_path() / ".bmx" / (name + opt.suffix + ".o");
}

auto SourceFile::add(boostfs::path path) -> SourceFile*
{
	for(auto& f : all){
		if(f->path == path){
			return f.get();
		}
	}
	all.push_back(make_unique<SourceFile>(boostfs::canonical(path)));

	return all.back().get();
}

auto SourceFile::serialize(vector<Buildable*>& v) -> bool
{
	parse();
	for(auto *src : imports) {
		src->parse();
	}
	for(auto *src : imports) {
		src->serialize(v);
	}
	if(find(cbegin(v), cend(v), this) == cend(v)){
		v.push_back(this);
	}
	return true;
}

Module::Module(std::string s) : id(move(s)), idl(id)
{
	lower_string(idl);
	main_src = SourceFile::add(dir() / (name()+".bmx"));
	main_src->mod = this;

	assert(id.find(".") != string::npos);
	assert(id.find(".", id.find(".") + 1) == string::npos);
}
Module::~Module()
{
}

auto Module::group() const -> std::string
{
	return id.substr(0, id.find("."));
}
auto Module::name() const -> std::string
{
	return id.substr(id.find(".") + 1);
}
auto Module::dir() const -> boostfs::path
{
	return modulepath(id);
}

auto Module::load_modules() -> void
{
	auto dir = modulepath();

	const auto end_it = boostfs::directory_iterator();
	for(auto it = boostfs::directory_iterator(dir); it != end_it; it++){
		const auto& entry = *it;
		auto group = entry.path().filename().string();
		
		if(!is_directory(entry.path()) || strcmp(group.c_str() + group.size() - 4, ".mod") != 0){
			continue;
		}

		group.resize(group.size() - 4);

		for(auto it2 = boostfs::directory_iterator(dir / entry.path().filename()); it2 != end_it; it2++){
			const auto& entry2 = *it2;
			auto name = entry2.path().filename().string();
			if (strcmp(name.c_str() + name.size() - 4, ".mod") != 0 || !is_directory(entry2.path())){
				continue;
			}
			name.resize(name.size() - 4);

			auto modid = group + "." + name;
			auto i = count(modid.cbegin(), modid.cend(), '.');

			if(i == 1) all.push_back(make_unique<Module>(modid));
			// cdebug << "Loaded Module " << modid << endl;
		}
	}
}

auto Module::ready(const OptionState& opt) const -> bool
{
	boostfs::path arc_file(dir() / (name() + opt.suffix + ".a"));
	boostfs::path iface_file(dir() / (name() + opt.suffix + ".i"));
	if(!boostfs::exists(arc_file) || !boostfs::exists(iface_file))
		return false;
	
	// TODO: improve this condition
	return boostfs::last_write_time(arc_file) > boostfs::last_write_time(main_src->path) &&
		boostfs::last_write_time(iface_file) > boostfs::last_write_time(main_src->path);
}
auto Module::build(const OptionState& opt) -> void
{
	vector<Buildable*> srcfiles;
	auto path = output_path(opt);
	auto arc = path.string();
	path.replace_extension(".i");
	auto info = path.string();
	string cmd("ar -r " + cquote(arc));

	main_src->serialize(srcfiles);

	boostfs::remove(arc);
	boostfs::remove(info);

	for(auto *b : srcfiles) {
		SourceFile *sf = dynamic_cast<SourceFile*>(b);
		if(opt.all || !sf->ready(opt)){
			sf->build(opt);
			cmd += " " + cquote(sf->output_path(opt).string());
		}
	}

	if(!boostfs::exists(info)){
		main_src->build(opt);
	}

	cout << "Archiving:" << output_path(opt).filename().string() << endl;
	exec(cmd + blobs);
	if(!boostfs::exists(arc) || !boostfs::exists(info)){
		throw runtime_error("Build Error: Failed to build module");
	}
}
auto Module::sources() const -> vector<boostfs::path>
{
	vector<boostfs::path> v;
	// FIXME: not implemented
	return v;
}
auto Module::outputs(const OptionState& opt) const -> vector<boostfs::path>
{
	vector<boostfs::path> v;
	v.push_back(dir() / (name() + opt.suffix + ".a"));
	v.push_back(dir() / (name() + opt.suffix + ".i"));
	return v;
}

auto Module::find_one(string search_id, Module** result) -> bool
{
	lower_string(search_id);
	for(auto& mod : all){
		if(mod->id == search_id){
			*result = mod.get();
			return true;
		}
	}
	
	return false;
}

auto Module::find_all(std::string search_id, vector<Module*> &result) -> bool
{
	auto sid_len = search_id.length();
	bool found = false;
	lower_string(search_id);

	for(auto& mod : all){
		if(mod->id.compare(0, sid_len, search_id) == 0){
			result.push_back(mod.get());
			found = true;
		}
	}
	
	return found;
}

auto Module::output_path(const OptionState& opt) const -> boostfs::path
{
	return dir() / (name() + opt.suffix + ".a");
}

// FIXME: this functionality should be in a different function
// Module::serialize should order the modules so that they can be built
// in that order
auto Module::serialize(vector<Buildable*>& v) -> bool
{
	vector<Buildable*> tmp;
	main_src->serialize(tmp);
	if(find(cbegin(v), cend(v), this) == cend(v)){
		v.push_back(this);
	}
	for(auto *b : tmp){
		auto *sf = dynamic_cast<SourceFile*>(b);
		for(auto *m : sf->modimports){
			m->serialize(v);
		}
	}
	return true;

}
auto Module::depends_on(const Module *m) const -> bool
{
	return main_src->depends_on(m);
}

App::App(const boostfs::path& p)
	: main_src(SourceFile::add(p))
{
}
App::~App()
{
}

auto App::output_path(const OptionState& opt) const -> boostfs::path
{
	boostfs::path p = main_src->path;
#ifdef _WIN32
	p.replace_extension(".exe");
#else
	p.replace_extension();
#endif
	return p;
}


auto App::ready(const OptionState& opt) const -> bool
{
	boostfs::path p = output_path(opt);
	if(!boostfs::exists(p))
		return false;
	
	return boostfs::last_write_time(p) > boostfs::last_write_time(main_src->path);
}

auto App::build(const OptionState& optBase) -> void
{
	OptionState opt = optBase;
	vector<Buildable*> srcfiles;
	vector<Buildable*> mods;
	Module *framework = nullptr;

	main_src->serialize(srcfiles);

	if(opt.framework.size() > 2){
		if(!Module::find_one(opt.framework, &framework)){
			throw runtime_error("cannot find framework "+opt.framework);
		}
	}else{
		main_src->parse();
		framework = main_src->framework;
		if(framework != nullptr){
			opt.framework = framework->modid();
		}
	}

	for(auto *b : srcfiles) {
		SourceFile *sf = dynamic_cast<SourceFile*>(b);
		if(sf == main_src){
			sf->ismain = true;
		}
		if(opt.all || !sf->ready(opt)){
			sf->build(opt);
		}
		for(Module *m : sf->modimports){
			mods.push_back(static_cast<Buildable*>(m));
			m->serialize(mods);
		}
	}

	/* FIXME: this is hardcoded for 32 bit compilation */
	string cmd, files, tmpfile("tmp.ld");

	boostfs::remove(opt.outfile);

	if(framework != nullptr)
		framework->serialize(mods);

	Module *appstub, *runtime;
	if(Module::find_one(opt.appstub, &appstub)){
		appstub->serialize(mods);
	}
	if(!Module::find_one(opt.runtime, &runtime)){
		throw runtime_error("cannot find runtime module "+opt.runtime);
	}

	auto order = [](const Buildable *a, const Buildable *b){
		const auto *p = dynamic_cast<const Module*>(a);
		const auto *q = dynamic_cast<const Module*>(b);
		assert(p != nullptr && q != nullptr);
		return p->depends_on(q);
	};
	partial_order(begin(mods), end(mods), order);
	assert(partial_ordered(begin(mods), end(mods), order));

	// runtime module needs to be last
	auto runtime_pos = find(begin(mods), end(mods), runtime);
	if(runtime_pos != begin(mods)){
		if(runtime_pos == end(mods)){
			mods.push_back(runtime);
		}else{
			std::rotate(runtime_pos, runtime_pos+1, end(mods));
		}
	}

	for(auto *b : srcfiles) {
		SourceFile *sf = dynamic_cast<SourceFile*>(b);
		assert(sf != nullptr);
		files += cquote(sf->output_path(opt)) + " ";
	}
	for(auto *b : mods) {
		Module *m = dynamic_cast<Module*>(b);
		assert(m != nullptr);
		files += cquote(m->output_path(opt)) + " ";
	}
	for(auto *b : srcfiles) {
		SourceFile *sf = dynamic_cast<SourceFile*>(b);
		assert(sf != nullptr);
		files += sf->libs + " ";
	}
	for(auto *b : mods) {
		Module *m = dynamic_cast<Module*>(b);
		assert(m != nullptr);
		if(m->libs.size() > 0){
			files += m->libs +" ";
		}
	}

#ifdef _WIN32
	const boostfs::path ld_path = get_bmxpath() / "bin" / "ld.exe";
	if(boostfs::exists(ld_path)){
		cmd=ld_path.string() + " --oformat pei-i386 -s -stack 4194304";
		if(opt.apptype=="gui")
			cmd += " -subsystem windows";

		if(opt.makelib)
			cmd += " -shared";

		cmd += " -o " + cquote(opt.outfile) + " " +
			cquote("-L"+cquote(get_bmxpath() / "lib"));

		if(opt.makelib){
			auto imp = boostfs::path(opt.outfile);
			imp.replace_extension(".a");
			auto def = boostfs::path(opt.outfile);
			def.replace_extension(".def");
			if(!boostfs::exists(def))
				throw runtime_error("cannot locate .def file");

			cmd += " " + def.string() + " --out-implib " + imp.string();
			files = cquote(get_bmxpath() / "lib" / "dllcrt2.o") + "\r\n" + files;
		}else{
			files = cquote(get_bmxpath() / "lib" / "crtbegin.o") + "\r\n" +
				cquote(get_bmxpath() / "lib" / "crt2.o") + "\r\n" + files;
		}
		
		// TODO: xpmanifest

		cmd += " "+cquote(tmpfile);

		files += "\r\n"+opt.libs;
		if(!opt.makelib)
			files += " "+cquote(get_bmxpath() / "lib" / "crtend.o");
		
	}else{
		cmd = opt.link + " " + opt.lflags + " -o" + cquote(opt.outfile) +
			" -L "+cquote(get_bmxpath() / "lib")+ " " + cquote(tmpfile) +
			" " + opt.libs;
	}
	files = "INPUT("+files+")\n";
#else	// UNIX
	cmd = opt.link+" "+opt.lflags+" -o "+cquote(opt.outfile)+" "+cquote(tmpfile) +
		" " + opt.libs + " -L"+cquote(get_bmxpath() / "lib");

	files = "INPUT("+files+")";
#endif

	auto* t = getenv("BMK_LD_OPTS");
	if(t != nullptr){
		cmd += " "s+t;
	}

	ofstream f;
	f.open(tmpfile.c_str());
	f << files;
	f.close();

	cout << "Linking:" << opt.outfile << endl;
	if(exec(cmd) != 0)
		throw runtime_error("Build error: failed to link");
}
auto App::sources() const -> std::vector<boostfs::path>
{
	vector<boostfs::path> v;
	// FIXME: not implemented
	return v;
}
auto App::outputs(const OptionState& opt) const -> std::vector<boostfs::path>
{
	return vector<boostfs::path>{output_path(opt)};
}
auto App::depends_on(const Module *m) const -> bool
{
	return main_src->depends_on(m);
}
