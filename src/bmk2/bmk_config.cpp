#include "bmk.h"
#include "bmk_config.h"
#include "bmk_util.h"
#include <cpptoml.h>
#include <thread>
using namespace std;

#define CONFIGFILE_NAME "bmk.toml"

OptionState::OptionState()
	: all(false), 
	  trackdeps(false),
	  verbose(false),
	  exec(false),
	  debug(true),
	  makelib(false),
	  release(true),
	  threaded(false),
	  arch("x86")
{
	apptype = "console";
	asflags = "-m1048560";
#ifdef _WIN32
	as = "fasm.exe";
	make = "mingw32-make.exe";
	cc = "gcc.exe";
	cxx = "g++.exe";
	link = "g++.exe";
	cflags = "-w -m32 -fno-exceptions -ffast-math -march=pentium";
	lflags = "-m32 -static -s ";
	libs = "-lgdi32 -lwinmm -lwsock32 -ladvapi32 -lpthread -lgcc -lmingwex -lmingw32 -lmoldname -lmsvcrt -luser32 -lkernel32";
#else
	as = "fasm";
	make = "make";
	cc = "gcc";
	cxx = "g++";
	cflags = "-w -m32 -fno-exceptions -mfancy-math-387 -fno-strict-aliasing";
	link = "g++";
	lflags = "-m32 -s -Os -pthread ";
	libs = "-Wl,-rpath='$ORIGIN' -L/usr/lib/i386-linux-gnu -L/usr/X11R6/lib -L/usr/lib";
#endif
}

OptionState opt;
SpecialOutput<const bool&> cverbose(opt.verbose);

void lowerstring(string& s)
{
	for(auto& x : s)
		x = tolower(x);
}
auto lowerstring(const char* s) -> string
{
	string tmp(s);
	lowerstring(tmp);
	return tmp;
}

void set_config_mung()
{
	if(opt.release){
		opt.debug = false;
	}else{
		opt.debug = true;
		opt.release = false;
	}
}

auto cleanmodules(const vector<string>& args)
{
	if(args.size() > 1)
		cmd_error();

	if(args.empty())
		opt.modfilter.clear();
	else
		setmodfilter(args[0]);

	/* TODO */

}

static std::shared_ptr<cpptoml::table> configfile;

template<typename T>
static auto set_opt_if(std::shared_ptr<cpptoml::table> conf, T *const option, const char *name) -> void
{
	auto o = conf->get_as<T>(name);
	if(o) *option = *o;
}

static auto apply_build_profile(const char* name) -> void
{
	if(!configfile) throw "cannot find config file!";
	auto build = configfile->get_table("build");
	if(!build) return;

	if(name != nullptr){
		build = build->get_table(name);
		if(!build)
			throw runtime_error("build profile not found");
	}
	set_opt_if(build, &opt.all, "all");
	set_opt_if(build, &opt.verbose, "verbose");
	set_opt_if(build, &opt.threaded, "threaded");
	set_opt_if(build, &opt.debug, "debug");
	set_opt_if(build, &opt.release, "release");
	set_opt_if(build, &opt.concurrent, "concurrent");
	set_opt_if(build, &opt.arch, "arch");
	set_opt_if(build, &opt.apptype, "apptype");
	set_opt_if(build, &opt.framework, "framework");
	set_opt_if(build, &opt.appstub, "appstub");
	set_opt_if(build, &opt.runtime, "runtime");
	set_opt_if(build, &opt.platform, "platform");

	set_opt_if(build, &opt.as, "as");
	set_opt_if(build, &opt.asflags, "asflags");
	set_opt_if(build, &opt.make, "make");
	set_opt_if(build, &opt.cc, "cc");
	set_opt_if(build, &opt.cxx, "cxx");
	set_opt_if(build, &opt.link, "link");
	set_opt_if(build, &opt.cflags, "cflags");
	set_opt_if(build, &opt.lflags, "lflags");
	set_opt_if(build, &opt.libs, "libs");
}

auto parse_config_file() -> void
{
	try{
		configfile = cpptoml::parse_file((get_bmxpath() / "bin" / CONFIGFILE_NAME).string());
	}catch(cpptoml::parse_exception& e){
		return;
	}
	try{
		apply_build_profile(nullptr);
#ifdef _WIN32
		apply_build_profile("win32");
#else
		apply_build_profile("linux");
#endif
	}catch(runtime_error& err){
	}
}

auto parse_config_args(int argc, char **argv) -> vector<string>
{
	int i;
	vector<string> args;

	opt.concurrent = thread::hardware_concurrency();

	for(i = 2; i < argc; i++){
		if(argv[i][0] != '-') break;
		switch(argv[i][1]){
		case 'a':
			opt.all = true;
			break;
		case 'b':
			if(++i == argc) cmd_error();
			opt.appstub = string(argv[i]);
			break;
		case 'd':
			opt.debug = true;
			opt.release = false;
			break;
		case 'D':
			opt.trackdeps = true;
			break;
		case 'f':
			if(++i == argc) cmd_error();
			opt.framework = string(argv[i]);
			break;
		case 'g':
			if(++i == argc) cmd_error();
			opt.arch = lowerstring(argv[i]);
			break;
		case 'h':
			opt.threaded = true;
			break;
		case 'j':
			if(++i == argc) cmd_error();
			opt.concurrent = atoi(argv[i]);
			break;
		case 'k':
			opt.kill = true;
			break;
		case 'o':
			if(++i == argc) cmd_error();
			opt.outfile = string(argv[i]);
			break;
		case 'P':
			if(++i == argc) cmd_error();
			apply_build_profile(argv[i]);
			break;
		case 'r':
			opt.release = true;
			opt.debug = false;
			break;
		case 'R':
			if(++i == argc) cmd_error();
			opt.runtime = argv[i];
			break;
		case 't':
			if(++i == argc) cmd_error();
			opt.apptype = lowerstring(argv[i]);
			break;
		case 'v':
			opt.verbose = true;
			break;
		case 'x':
			opt.exec = true;
			break;
		case 'z':
			opt.traceh = true;
			break;
		default:
			cmd_error();
			break;	
		}
	}

	if(opt.concurrent <= 0 || opt.concurrent > 16)
		opt.concurrent = 1;

	if(i < argc){
		args.reserve(argc-i);
		for(; i < argc; i++)
			args.emplace_back(argv[i]);
	}

	if(opt.apptype != "console" && opt.apptype != "gui"){
		throw "apptype must be either console or gui";
	}

	if(opt.runtime.size() == 0)
		throw "no runtime module configured";

	return args;
}
