#include "bmk.h"
#include "bmk_build.h"
#include "bmk_config.h"
#include "bmk_util.h"
#include "bmk_fs.h"
#include <cstdlib>
#include <cstring>

#ifdef _WIN32
#define BMX_STANDARD_PATH	"C:\\BlitzMax"
#else
#define BMX_STANDARD_PATH	"/opt/blitzmax"
#endif

using namespace std;

auto cmd_error() -> void
{
	cerr << "Command line error" << endl;
}

auto get_extensionl(const boostfs::path& p) -> std::string
{
	string s = p.extension().string();
	for(auto& x : s)
		x = tolower(x);
	return s;
}

auto trim(std::string& s) -> std::string&
{
	auto first_nonspace = s.find_first_not_of(" \t");
	if(first_nonspace != string::npos){
		s.erase(0, s.find_first_not_of(" \t"));
		auto last_nonspace = s.find_last_not_of(" \t\n\r");
		if(last_nonspace != string::npos){
			s.erase(last_nonspace+1);
		}
	}else{
		s.clear();
	}
	return s;
}


static boostfs::path BMXPATH(".");

static auto search_bmxpath() -> void
{
	char *s = getenv("BMXPATH");
	if(s != nullptr){
		BMXPATH = s;
	}

	if(!boostfs::exists(BMXPATH)){
		BMXPATH = boostfs::canonical(".");
		while(!BMXPATH.empty()){
			auto moddir = BMXPATH / "mod";
			if(boostfs::exists(moddir))
				break;
			BMXPATH = BMXPATH.parent_path();
		}
	}

	if(!boostfs::exists(BMXPATH)){
		BMXPATH = BMX_STANDARD_PATH;
	}

	if(!boostfs::exists(BMXPATH)){
		throw "could not find installation directory!";
	}
}

auto get_bmxpath() -> const boostfs::path&
{
	if(BMXPATH == "."){
		BMXPATH.clear();
		search_bmxpath();
	}

	return BMXPATH;
}


/*
	moduledir() returns the module directory form of a
	module name (e.g. returns 'pub.mod/stdc.mod' for
	input 'pub.stdc')
*/
auto moduledir(const string& mod) -> boostfs::path
{
	auto pos = mod.find('.');

	boostfs::path d = mod.substr(0, pos) + ".mod";
	d /= mod.substr(pos+1) + ".mod";

	return d;
}


/*
	modulepath() returns the full path of a module
	if given an empty string or no parameter,
	it returns the path of the BlitzMax mod directory
*/
auto modulepath() -> boostfs::path
{
	return boostfs::path(get_bmxpath()) / "mod";
}
auto modulepath(const string& mod) -> boostfs::path
{
	auto p = modulepath();

	if(mod.length() > 0 && mod.find('.') != string::npos){
		p /= moduledir(mod);
	}

	return p.string();
}

/*
	moduleident returns the module's own name
	(e.g. returns 'stdc' for input 'pub.stdc')
*/
auto moduleident(const string& mod) -> string
{
	return mod.substr(mod.find_last_of('.')+1);
}

auto setmodfilter(string t) -> void
{
	for(auto& x : t)
		x = tolower(x);

	if(t == "*"){
		opt.modfilter.clear();
	}else{
		opt.modfilter = move(t);
		if(!opt.modfilter.empty() && opt.modfilter.back() != '.'){
			opt.modfilter += '.';
		}
	}
}


auto cquote(string t) -> string
{
	static const string ok_chars("./_-\\");

	if(!t.empty() && t[0] == '-') 
		return t;

	for(const auto& c : t){
		if(!isalnum(c) && ok_chars.find(c) == string::npos)
			return "\""+t+"\"";
	}
	return t;
}

auto cquote(const boostfs::path& t) -> string
{
	return cquote(t.string());
}

auto exec(const string& cmd) -> int
{
	cverbose << cmd << std::endl;
	
	return system(cmd.c_str());
}

auto createarc(boostfs::path path, const vector<boostfs::path>& oobjs) -> void
{
	string cmd("ar -r " + cquote(path.string()));
	boostfs::remove_all(path);

	for(const auto& t : oobjs){
		cmd += " " + cquote(t);
	}

	if(cmd.size() > 0 && exec(cmd) != 0){
		boostfs::remove_all(path);
		throw "Build Error: Failed to create archive";
	}
}

auto endswith(const std::string& s1, const char *s2) -> bool
{
	return s1.rfind(s2) == s1.size() - strlen(s2);
}
