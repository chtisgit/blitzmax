#ifndef __BMK__
#define __BMK__

#include <iostream>

#ifndef NDEBUG
#define DEBUG(X)	X
#define DEBUG_YES	1
#else
#define DEBUG(X)
#define DEBUG_YES	0
#endif

#define DEBUG_MSG(X)	DEBUG(cerr << X)

template <typename TT>
struct SpecialOutput{

	TT truth_val;

	SpecialOutput(TT b) : truth_val(b)
	{
	}

	template<typename T>
	auto operator<<(const T& x) -> SpecialOutput&
	{
		if(truth_val){
			std::cout << x;
		}
		return *this;
	}
	auto operator<<(std::ostream& (*f)(std::ostream& os)) -> SpecialOutput&
	{
		if(truth_val){
			std::cout << f;
		}
		return *this;
	}
};

extern SpecialOutput<int> cdebug;

#endif

