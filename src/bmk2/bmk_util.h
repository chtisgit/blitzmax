#ifndef __BMK_UTIL__
#define __BMK_UTIL__

#include "bmk_fs.h"
#include "bmk_build.h"
#include <string>
#include <vector>

auto exec(const std::string& cmd) -> int;
auto get_bmxpath() -> const boostfs::path&;
auto cmd_error() -> void;
auto cquote(std::string) -> std::string;
auto cquote(const boostfs::path& t) -> std::string;
auto get_extensionl(const boostfs::path& p) -> std::string;
auto moduledir(const std::string&) -> boostfs::path;
auto modulepath() -> boostfs::path;
auto modulepath(const std::string&) -> boostfs::path;
auto moduleident(const std::string&) -> std::string;
auto setmodfilter(std::string t) -> void;
auto createarc(boostfs::path path, const std::vector<boostfs::path>& oobjs) -> void;
auto get_extensionl(const boostfs::path& p) -> std::string;
auto trim(std::string& s) -> std::string&;
auto endswith(const std::string& s1, const char *s2) -> bool;


inline auto lower_string(std::string& data) -> std::string&
{
	std::transform(data.begin(), data.end(), data.begin(), ::tolower);
	return data;
}

// partial order is established when all elementes are not smaller than their predecessors
template<typename RandomAccessIterator, typename Compare>
inline auto partial_ordered(RandomAccessIterator first, RandomAccessIterator last, Compare test) -> bool
{
	for(; first != last; ++first){
		for(auto p = first+1; p != last; ++p){
			// is p < first?
			if(test(*p, *first)){
				return false;
			}
		}
	}
	return true;
}

// this function imposes a partial ordering upon a sequence
// test(p,q) returns true if p < q and false if it does not care
template<typename RandomAccessIterator, typename Compare>
inline auto partial_order(RandomAccessIterator first, RandomAccessIterator last, Compare test) -> void
{
	std::vector<int> meta(last-first, 0);

	auto p = first;

	// init vector
	for(; p != last; ++p){
		for(auto q = first; q != last; ++q){
			if(test(*p, *q)){
				++meta[q-first];
			}
		}
	}

	size_t offset = 0;
	do{
		auto q = first;
		for(p = q; p != last; ++p){
			if(meta[p-first+offset] == 0){
				// bring the element to the front of the remaining list
				if(p != q){
					std::swap(meta[p-first+offset], meta[q-first+offset]);
					std::swap(*p, *q);
				}
				++q;
			}
		}
		// there was no element with zero dependencies left
		if(q == first)
			throw std::runtime_error("circular dependency found");

		// decrement the meta value of all elements that were 
		// dependent on the removed elements
		for(auto i = first; i != q; ++i){
			for(auto j = q; j != last ; ++j){
				if(test(*i, *j)){
					--meta[j-first+offset];
				}
			}
		}
		offset += q-first;
		first = q;
	}while(first != last);
}

#endif

