#ifndef BMK_BUILD
#define BMK_BUILD

#include <string>
#include <vector>
#include <memory>

#include "bmk_config.h"
#include "bmk_fs.h"

struct SourceFile;
class Module;
struct Build;

struct Buildable {
	Buildable();
	virtual ~Buildable();
	virtual auto ready(const OptionState& opt) const -> bool;
	virtual auto build(const OptionState& opt) -> void;
	virtual auto sources() const -> std::vector<boostfs::path>;
	virtual auto outputs(const OptionState& opt) const -> std::vector<boostfs::path>;
	virtual auto serialize(std::vector<Buildable*>&) -> bool;
	virtual auto depends_on(const Module *m) const -> bool;
};

struct SourceFile : public Buildable {
	static std::vector<std::unique_ptr<SourceFile>> all;

	boostfs::path path;
	std::string ext;
	Module *mod = nullptr;
	Module *framework = nullptr;
	bool parsed = false;
	bool ismain = false;

	std::vector<SourceFile*> imports;
	std::vector<Module*> modimports;
	std::vector<boostfs::path> incbins;
	std::vector<SourceFile*> includes;
	std::vector<std::string> info;
	std::string libs;

	explicit SourceFile(boostfs::path);
	virtual ~SourceFile();

	virtual auto ready(const OptionState& opt) const -> bool;
	virtual auto build(const OptionState& opt) -> void;
	virtual auto sources() const -> std::vector<boostfs::path>;
	virtual auto outputs(const OptionState& opt) const -> std::vector<boostfs::path>;
	virtual auto serialize(std::vector<Buildable*>&) -> bool;
	virtual auto depends_on(const Module *m) const -> bool;

	auto parse() -> void;
	auto valid_ext() -> bool;
	auto output_path(const OptionState& opt) const -> boostfs::path;
	auto makebmx(const OptionState& opt) -> void;
	auto makeC(const OptionState& opt) -> void;
	auto makeCpp(const OptionState& opt) -> void;

	static auto add(boostfs::path) -> SourceFile*;
	static auto assemble(const boostfs::path& src, const boostfs::path& dst, const OptionState&) -> void;
};

class Module : public Buildable{
	static std::vector<std::unique_ptr<Module>> all;

	std::string id;
	std::string idl;
	SourceFile *main_src;

public:
	std::string cc_opts;
	std::string blobs;
	std::string libs;

	explicit Module(std::string);
	virtual ~Module();
	auto group() const -> std::string;
	auto name() const -> std::string;
	auto dir() const -> boostfs::path;
	auto modid() const -> const std::string&
	{
		return id;
	}
	auto main_file() const -> SourceFile*
	{
		return SourceFile::add(dir() / (name() + ".bmx"));
	}
	auto operator==(const Module &m) const -> bool
	{
		return main_src == m.main_src;
	}
	auto output_path(const OptionState& opt) const -> boostfs::path;
	auto examine() -> void
	{
		main_src->parse();
		for(auto *src : main_src->imports) {
			src->parse();
		}
	}
	auto mainsource() -> const SourceFile*
	{
		return main_src;
	}

	virtual auto ready(const OptionState& opt) const -> bool;
	virtual auto build(const OptionState& opt) -> void;
	virtual auto sources() const -> std::vector<boostfs::path>;
	virtual auto outputs(const OptionState& opt) const -> std::vector<boostfs::path>;
	virtual auto serialize(std::vector<Buildable*>&) -> bool;
	virtual auto depends_on(const Module *m) const -> bool;

	static auto load_modules() -> void;
	static auto find_one(std::string search_id, Module** result) -> bool;
	static auto find_all(std::string search_id, std::vector<Module*>& result) -> bool;
};

class App : public Buildable{
	SourceFile *main_src;

	auto link(const OptionState& opt) -> void;
public:
	App(const boostfs::path& srcfile);
	virtual ~App();

	virtual auto ready(const OptionState& opt) const -> bool;
	virtual auto build(const OptionState& opt) -> void;
	virtual auto sources() const -> std::vector<boostfs::path>;
	virtual auto outputs(const OptionState& opt) const -> std::vector<boostfs::path>;
	virtual auto depends_on(const Module *m) const -> bool;

	auto output_path(const OptionState& opt) const -> boostfs::path;
};

#endif
