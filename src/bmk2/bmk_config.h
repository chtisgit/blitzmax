#ifndef __BMK_CONFIG__
#define __BMK_CONFIG__

#include <cstdint>
#include <iostream>
#include <string>
#include <vector>

#ifdef __linux__
#define HOST_PLATFORM	"linux"
#endif
#ifdef _WIN32
#define HOST_PLATFORM	"win32"
#endif

extern struct OptionState{
	bool all	= false;
	//bool quiet	= true;
	bool trackdeps	= false;
	bool verbose	= false;
	bool exec	= false;
	bool debug	= true;
	bool makelib	= false;
	bool release	= true;
	bool threaded	= false;
	bool kill	= false;
	bool traceh	= false;
	int concurrent	= 0;
	std::string arch;
	std::string outfile;
	std::string apptype;
	std::string framework;
	std::string appstub;
	std::string runtime;
	std::string platform = HOST_PLATFORM;
	std::string modfilter = ".";
	std::string as, asflags, make, cc, cxx, cflags, link, lflags, libs;

	std::string suffix;

	OptionState();

	auto gen_suffix() -> void
	{
		suffix = std::string(".")+(release ? "release" : "debug")+
			(threaded ? ".mt" : "")+"."+platform+"."+arch;
	}
} opt;

extern SpecialOutput<const bool&> cverbose;

void lowerstring(std::string&);
auto lowerstring(const char*) -> std::string;

auto gen_suffix(const std::string& buildtype, const std::string& os, const std::string& arch, bool t) -> std::string;
void set_config_mung();
auto parse_config_file() -> void;
auto parse_config_args(int, char **) -> std::vector<std::string>;

#endif
