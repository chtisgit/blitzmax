#include "bmk.h"
#include "bmk_config.h"
#include "bmk_util.h"

#include <cassert>
#include <cctype>
#include <iostream>
#include <functional>
#include <unordered_map>
using namespace std;

SpecialOutput<int> cdebug(DEBUG_YES);

inline auto _stub() -> void
{
	cerr << "Not implemented." << endl;
}

auto make_application(const vector<string>& args, bool library) -> void
{
	auto mainf = boostfs::complete(args[0]);
	const auto ext = boostfs::extension(mainf);

	if(ext.size() == 0){
		mainf += ".bmx";
	}else if(ext == ".c" || ext == ".cpp" || ext == ".cxx" || ext == ".mm" || ext == ".bmx"){
	}else{
		throw "Unrecognized app source file type";
	}

	if(!boostfs::is_regular_file(mainf))
		throw "Unable to open main source file";
	
	if(opt.outfile.size() == 0)
		opt.outfile = mainf.stem().string();
	
	if(opt.debug)
		opt.outfile += ".debug";
#ifdef _WIN32
	const auto outext = get_extensionl(opt.outfile);
	if(opt.makelib){
		if(outext != "dll") opt.outfile += ".dll";
	}else{
		if(outext != "exe") opt.outfile += ".exe";
	}
#endif
	App app(mainf);
	app.build(opt);

	if(opt.exec){
		cout << "Executing: " << opt.outfile << endl;
		auto cmd = cquote(opt.outfile);
#ifndef _WIN32
		if(cmd.find('/') == string::npos)
			cmd = "./"+cmd;
#endif
		for(size_t i = 1; i < args.size(); i++){
			cmd += " " + cquote(args[i]);
		}

		exec(cmd);
	}

}

auto make_modules(const vector<string>& args) -> void
{
	vector<Module*> modules;
	if(args.size() == 0){
		Module::find_all("", modules);
	}else{
		for(const auto& id : args){
			if(!Module::find_all(id, modules))
				throw runtime_error("no modules matching '"+id+"' were found");
		}
	}

	// parse modules' source code to find ordering later
	for(auto *mod : modules){
		mod->examine();
	}

	auto order = [](const Module *p, const Module *q){
		return q->depends_on(p);
	};
	partial_order(begin(modules), end(modules), order);
	assert(partial_ordered(begin(modules), end(modules), order));

	// we need to build the runtime module first, so we bring it to
	// the front of the modules vector unless it is already up-to-date
	Module *core;
	if(!Module::find_one(opt.runtime, &core))
		throw runtime_error("could not find "+opt.runtime);

	auto core_pos = find(begin(modules), end(modules), core);
	if(!core->ready(opt) && core_pos != begin(modules)){
		if(core_pos == end(modules)){
			modules.insert(begin(modules), core);
		}else{
			std::rotate(begin(modules), core_pos, core_pos+1);
		}
	}

	for(auto *mod : modules){
		mod->build(opt);
	}
}


auto cmd_makeapp(const vector<string>& args) -> int
{
	if(opt.exec){
		if(args.empty()) cmd_error();
	}else{
		if(args.size() != 1) cmd_error();
	}

	if(opt.release && opt.debug)
		opt.debug = false;

	opt.gen_suffix();
	make_application(args, false);
	return 0;
}

auto cmd_makemods(const vector<string>& args) -> int
{
	if(opt.exec){
		cmd_error();
	}

	if(opt.release && opt.debug){
		opt.debug = true;
		opt.release = false;
		opt.gen_suffix();
		make_modules(args);

		opt.debug = false;
		opt.release = true;
		opt.gen_suffix();
		make_modules(args);
	}else{
		opt.gen_suffix();
		make_modules(args);
	}
	return 0;
}

auto cmd_makelib(const vector<string>& args) -> int
{
	_stub();
	return 0;
}

auto cmd_cleanmods(const vector<string>& args) -> int
{

	vector<Module*> modules;
	if(args.size() == 0){
		Module::find_all("", modules);
	}else if(!Module::find_all(args[0], modules)){
		return 0;
	}
	
	for(Module *m : modules){
		boostfs::remove_all(m->dir() / ".bmx");
	}
	return 0;
}

auto cmd_zapmod(const vector<string>& args) -> int
{
	_stub();
	return 0;
}


auto cmd_unzapmod(const vector<string>& args) -> int
{
	_stub();
	return 0;
}


auto cmd_listmods(const vector<string>& args) -> int
{
	vector<Module*> modules;
	if(args.size() == 0){
		Module::find_all("", modules);
	}else if(!Module::find_all(args[0], modules)){
		return 0;
	}
	
	for(Module *m : modules){
		cout << m->modid() << endl;
	}

	return 0;
}


auto cmd_modstatus(const vector<string>& args) -> int
{
	_stub();
	return 0;
}


auto cmd_syncmods(const vector<string>& args) -> int
{
	_stub();
	return 0;
}


auto cmd_convertbb(const vector<string>& args) -> int
{
	_stub();
	return 0;
}


auto cmd_ranlibdir(const vector<string>& args) -> int
{
	_stub();
	return 0;
}

unordered_map<string,function<int(const vector<string>&)>> command_map = {
		{string("makeapp"), cmd_makeapp},
		{string("makemods"), cmd_makemods},
		{string("makelib"), cmd_makelib},
		{string("cleanmods"), cmd_cleanmods},
		{string("zapmod"), cmd_zapmod},
		{string("unzapmod"), cmd_unzapmod},
		{string("listmods"), cmd_listmods},
		{string("modstatus"), cmd_modstatus},
		{string("syncmods"), cmd_syncmods},
		{string("convertbb"), cmd_convertbb},
		{string("ranlibdir"), cmd_ranlibdir}
	};


auto main(int argc, char **argv) -> int
{
	auto error = 1;
	if(argc < 2){
		cmd_error();
		return 1;
	}

	try{
		Module::load_modules();

		parse_config_file();
		string cmd(argv[1]);
		auto args = parse_config_args(argc, argv);
		// invoking corresponding function
		error = command_map.at(cmd)(args);
	}catch(const char *s){
		cerr << "fatal error: " << s << endl;
	}catch(const std::exception& e){
		cerr << "runtime error: " << e.what() << endl;
	}catch(...){
		cmd_error();
	}
	
	return error;
}

